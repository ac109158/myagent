

<?php
	require_once './html/header.html.php'; // setup html document w/ and demo requirements
// Container for agent is now set 
?>



	<div id="upper-profile-section" style="height:165px;"> <!-- Sets upper section of the agent that should always be displayed --> 
<?php
		require_once './html/profile.html.php'; // import profile content	
?>
	</div> <!-- end of profile section --> 
	<div id="full-display-section" style="height:305px;"><!-- defines the content display section --> 

	</div> <!-- end full-display-section --> 



<?php
// container closed
require_once './html/bottom.html.php';
require_once './html/footer.html.php';
?>
<br>


<script>
var queueTimers = []; // Holds all of the timer objects for the queue
$('.settings').hide();


// This function is used to alternate the the backround of line rows
function paintList(selectorUL) 
{
	console.log(selectorUL);
	$(selectorUL + " > li").filter(":nth-child(odd)").css("background", "#f2f2f2").end().filter(":nth-child(even)").css("background", "#e0e0e0");	
}
/////


$("#activate").click(function () 
{
	
    $('.container').toggle("drop", { direction: "left" }, 500);
    $('.settings').hide();
});


 // DEMO ONLY .. triggers a new call into the queue
$("#call-in").click(function () 
{
	$("#queue-list > #empty").remove();
	$("#queue-list").append('<li>iFocus Radio <span class="timer">0:00</span></li>');
	queueTimers.push(setTimer($('#queue-list li:last').find('.timer')));
	// Update the count of the calls in queue
	queueCounter('#queue-list');
	paintList('#queue-list');
});



// DEMO ONLY .. removes the top call from the queue list
$("#call-out").click(function () 
{
	$('#queue-list li:first').fadeOut("slow").remove();
	clearInterval(queueTimers.shift());
	queueCounter();
	if ($('#queue-count').text() == '0') 
	{
		$("#queue-list").append('<li id="empty"><center>Queue is empty</center></span></li>');
	}
	paintList('#queue-list');
});

function queueCounter() 
{
	// Update the count of the calls in queue
	$("#queue-count").text($('#queue-list li').size());
	if ($('#queue-list li').size() >= 5) 
	{
		blink('#queue-count');
	};
	if ($('#queue-list li').size() >= 10) 
	{
		$('#queue-count').css({"background-color":"red", "color":"white"});
	}
	else 
	{
		$('#queue-count').css({"background-color":"white", "color":"black"});
	};

}
	


</script>



