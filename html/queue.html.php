<div class="queue"> 
	<div class="queue-header">
		<img class="queue-icon" src="images/queue.png"/>
		<h4>Queue</h4>
		<a href="#queue-toggle"><img id="queue-toggle" src="images/arrow-up.png"/></a>
		<span id="queue-count">3</span>
	</div>
	<div class="queue-body">
		<ul id="queue-list">
			<li>NoNo Radio <span class="timer">0:00</span></li>
			<li>iFocus Radio <span class="timer">0:00</span></li>
			<li style="border-bottom:0px;">Rosetta Stone <span class="timer">0:00</span></li>
		</ul>
	</div>
</div>

<script>
var queueTimers = [];
$('#queue-toggle').click(function() 
{
	$(".queue-body").slideToggle(100);
	var src = ($(this).attr('src') === 'images/arrow-up.png') ? 'images/arrow-down.png' : 'images/arrow-up.png';
	$(this).attr('src', src); 
	return false;

})					
</script>