<div class="bottom-menu">
	<div class="icon-wrapper">
		<a href="#"><img id="queue-link" class="tad8-up" src="images/queue.png" alt="Queue"/></a>
		<a href="#"><img id="history-link" src="images/call-history-2.png" alt="Call History"/></a>
		<a href="#"><img id="address-link" src="images/address-book.png" alt="Address Book"/></a>
		<a href="#"><img id="feedback-link"src="images/feedback.png" alt="Feedback"/></a>
	</div>
	<a id="power-off" href="#"><img src="images/power.png"/></a>
</div>

<script>
$('#queue-link').click(function() 
{
	$('#full-display-section').empty();
	$("#full-display-section" ).load( "html/queue.html.php" , function() {
	$("#queue-list").empty();
	$('#queue-count').text('0');
	$("#queue-list").append('<li id="empty"><center>Queue is empty</center></span></li>');
});
});

$('#history-link').click(function() 
{
	$('#full-display-section').empty();
	$("#full-display-section" ).load( "html/history.html.php" );
});

$('#address-link').click(function() 
{
	$('#full-display-section').empty();
	$("#full-display-section" ).load( "html/address.html.php" );
});

$('#feedback-link').click(function() 
{
	$('#full-display-section').empty();
	$("#full-display-section" ).load( "html/feedback.html.php" );
	$('.feedback-body').css('min-height', '270px');
});
</script>

