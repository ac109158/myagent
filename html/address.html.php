			<div class="address">
				<div class="address-header">
					<img src="images/address-book-2.png"/>
					<h4>Address Book</h4>
					<a href="#"><img id="address-toggle" src="images/arrow-up.png"/></a>
				</div>
				<div class="address-body">
						<div class="contact-filters"> 

							<!--                   SEARCH/FILTER                                -->

						    <img src="images/address-search.png"/><img src="images/address-filter.png"/>
							<select class="address-filter">
								<img href="images/arrow-down.png"/>
								<option value="offline">Offline</option>
								<option value="available">Available</option>
								<option value="unavailable">Unavailable</option>
								<option value="break">Break</option>
								<option value="wrap">Wrap</option>
							</select><br/>


							<!--                          TEAM/SKILLS                                -->
			
							<img src="images/address-campaigns.png"/><img src="images/address-teams.png"/>
							<select class="address-team">
								<img href="images/arrow-down.png"/>
								<option value="offline">Offline</option>
								<option value="available">Available</option>
								<option value="unavailable">Unavailable</option>
								<option value="break">Break</option>
								<option value="wrap">Wrap</option>
							</select>
						</div>
						<div class="address-list-header">
							Name <span>Status</span>
						</div>
						<ul>
							<li>Michelle Timpson <span>0:00 <div></div></span></li>
							<li>Calvin Wayman <span>0:00 <div></div></span></li>
							<li>Calvin Wayman <span>0:00 <div></div></span></li>
							<li>Calvin Wayman <span>0:00 <div></div></span></li>
							<li>Calvin Wayman <span>0:00 <div></div></span></li>
							<li>Calvin Wayman <span>0:00 <div></div></span></li>
							<li>Calvin Wayman <span>0:00 <div></div></span></li>
							<li style="border-bottom:0px;">Becky Roundy <span>0:00 <div></div></span></li>
						</ul>
				</div>
			</div>



<script>
(function($) {
    $.fn.selected = function(fn) {
        return this.each(function() {
            var clicknum = 0;
            $(this).click(function() {
                clicknum++;
                if (clicknum == 2) {
                    clicknum = 0;
                    fn(this);
                }
            });
        });
    }
})(jQuery);

$('#address-toggle').click(function() 
{
	$(".address-body").slideToggle(100);
	var src = ($(this).attr('src') === 'images/arrow-up.png') ? 'images/arrow-down.png' : 'images/arrow-up.png';
	$(this).attr('src', src); 
	return false;

})					



$('.address-team').selected(function() {
	console.log($('.address-team')[0].value);
});	

$('.address-filter').selected(function() {
    console.log($('.address-filter')[0].value);
});					

</script>