<STYLE TYPE="text/css">
#icon-nav {
	background-color: #ccc;
	height:25px;
	width:270px;
	padding: 5px;
	 margin-bottom: 5px;
}
#icon-nav ul {
	list-style-type: none;
	padding: 0;
	margin-top: 0px;
}

.icon-nav-link{
	width:20px;
	height:25px;
	float:left;
	<!-- background-color: black; --> 
	margin: 0;
	margin-right: 5px;
}

.pull-right {
	margin: 0;
	width:20px;
	height:25px;
	float:right;
}
#icon-nav  #multiselect {
    position:relative;
    background-color: #ccc;
    top:30px;
    left:-50px;
    width:96px;
    padding:1px;
    height:190px;
    border:solid 1px #c0c0c0;
    overflow:auto;
}

.hidden {
	display:none;
}
 
#icon-nav #multiselect label {
    display:block;
}

.setting{
	width:80%;
    text-decoration: none;
    -ms-text-underline-position: none;
    float: right;
}
 
.#icon-nav multiselect-on {
    color:#ffffff;
    background-color:#000099;
}

</STYLE>


<div id="icon-nav">
	<div id="icon-wrapper" style="width:100%; height:25px; ">
		<ul id="icon-list">
			<li class="icon-nav-link"><a href="#"><img src="images/queue.png"/></li>
			<li class="icon-nav-link"><a href="#"><img src="images/phone.png"/></li>
			<li class="icon-nav-link"><a href="#"><img src="images/phone.png"/></li>
			<li class="icon-nav-link"><a href="#"><img src="images/phone.png"/></li>
			<li class="icon-nav-link"><a href="#"><img src="images/phone.png"/></li>
			<li class="icon-nav-link"><a href="#"><img src="images/phone.png"/></li>
			<li class="icon-nav-link"><a href="#"><img src="images/phone.png"/></li>
			<li class="icon-nav-link"><a href="#"><img src="images/phone.png"/></li>
			<li class="icon-nav-link"><a href="#"><img src="images/power.png"/></li>
			<li id="icon-setting" class="pull-right"><a href="#"><img src="images/settings.png"/></li>

			
		</ul> <!-- end of icon-list --> 
		<div id="multiselect" class="hidden">
		    <span><input type="checkbox" name="option[]" value="1" /><label class="setting">Queue</label><span>
		    <input type="checkbox" name="option[]" value="2" /><label class="setting">Phone</label>
		    <input type="checkbox" name="option[]" value="3" /><label class="setting">Calls</label>
		    <input type="checkbox" name="option[]" value="4" /><label class="setting">Contacts</label>
		    <input type="checkbox" name="option[]" value="5" /><label class="setting">Agents</label>
		    <input type="checkbox" name="option[]" value="6" /><label class="setting">Alert</label>
		    <input type="checkbox" name="option[]" value="7" /><label class="setting">Reports</label>
		    <input type="checkbox" name="option[]" value="8" /><label class="setting">Feedback</label>
		</div>
	</div> <!-- end of icon_wrapper --> 
	
</div>
<script>
jQuery.fn.multiselect = function() {
    $(this).each(function() {
        var checkboxes = $(this).find("input:checkbox");
        checkboxes.each(function() {
            var checkbox = $(this);
            // Highlight pre-selected checkboxes
            if (checkbox.attr("checked"))
                checkbox.parent().addClass("multiselect-on");
 
            // Highlight checkboxes that the user selects
            checkbox.click(function() {
                if (checkbox.attr("checked"))
                    checkbox.parent().addClass("multiselect-on");
                else
                    checkbox.parent().removeClass("multiselect-on");
            });
        });
    });
};

$('#icon-setting').click(function () 
{
	$('#multiselect').toggleClass('hidden');

});

</script>