<div class="feedback">
	<div class="feedback-header">
		<img src="images/feedback-2.png"/>
		<h4>Feedback</h4>
		<a href="#"><img id="queue-toggle" src="images/arrow-up.png"/></a>
	</div>
	<div id="feedback-wrapper">
		<div class="feedback-body">
			<div id="feedback-content">
			<h4>Category</h4>
			<select>
				<option value="none">Select</option>
			</select>
			<h4>Priority</h4>
			<select>
				<option value="none">Select</option>
			</select>
			<h4>Comments</h4>
			<textarea></textarea>
			</div>
		</div>
	</div>
</div>
<script>
$('#queue-toggle').click(function() 
{
	$(".feedback-body").slideToggle(100);
	var src = ($(this).attr('src') === 'images/arrow-up.png') ? 'images/arrow-down.png' : 'images/arrow-up.png';
	$(this).attr('src', src); 
	return false;

})					
</script>