<div class="conversation">
	<div class="conversation-header">
		<img src="images/conversation.png"/>
		<h4>Conversation</h4>
		<a href="#"><img id="conversation-toggle" src="images/arrow-up.png"/></a>
	</div>
	<div class="conversation-body" id="conversation-body">
		<img src="images/phone-blue.png"/>
		<span>NoNo Outbound</span>
		<span style="float:right;">0:00</span>
		<hr>
		<div class="call-status">
			<span class="call-status-title">Dialing</span><span style="float:right;">0:00</span>
		</div>
		<div class="conversation-buttons">
			<a href="#"><img src="images/hold.png"/></a>
			<a href="#"><img src="images/mute.png"/></a>
			<a href="#"><img src="images/hang-up.png"/></a>
			<a href="#"><img src="images/call-back.png"/></a>
		</div>
	</div>
</div>
<script>
$('#conversation-toggle').click(function() 
{
	$(".conversation-body").slideToggle(100);
	var src = ($(this).attr('src') === 'images/arrow-up.png') ? 'images/arrow-down.png' : 'images/arrow-up.png';
	$(this).attr('src', src); 
	return false;

})
</script>					
