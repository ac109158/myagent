<div class="call-history">
	<div class="history-header">
		<img src="images/call-history-3.png"/>
		<h4>Call History</h4>
		<a href="#"><img id="queue-toggle" src="images/arrow-up.png"/></a>
	</div>
	<div class="history-body">
		<ul>
			<li>NoNo Radio <span>0:00</span></li>
			<li>iFocus Radio <span>0:00</span></li>
			<li>NoNo Radio <span>0:00</span></li>
			<li>NoNo Radio <span>0:00</span></li>
			<li>iFocus Radio <span>0:00</span></li>
			<li>NoNo Radio <span>0:00</span></li>
			<li style="border-bottom:0px;">Rosetta Stone <span>0:00</span></li>
		</ul>
	</div>
</div>
<script>
$('#queue-toggle').click(function() 
{
	$(".history-body").slideToggle(100);
	var src = ($(this).attr('src') === 'images/arrow-up.png') ? 'images/arrow-down.png' : 'images/arrow-up.png';
	$(this).attr('src', src); 
	return false;

})					
</script>