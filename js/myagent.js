function setStatus()
{
	if(typeof(agentTimer)!= "undefined" && agentTimer !== null) 
	{
		clearInterval(agentTimer);
	}
	$('#agent-timer').text("0:00");
	var input = $('.myagent').val();
	if (input !== "offline") 
	{
		agentTimer = setTimer('#agent-timer');
	}
	$('#agent-timer').css("color","white");
	$('#full-display-section').empty();
	switch (input) 
	{
		case 'available':
			$('.myagent-wrapper').css("background-color","#2eaf2e");//green
			case 'available':
			$( "#full-display-section" ).load( "html/inner.html.php", function() {
			$( "#upper-content-section" ).load( "html/conversation.html.php", function() {
			$( "#lower-content-section" ).load( "html/queue.html.php", function() {
			$( ".queue-body").css("height","140px");
			//load succesful
			});
			});
			})
			break;
		case 'unavailable':
			$('.myagent-wrapper').css("background-color","#af2e2e"); //red
			$( "#full-display-section" ).load( "html/queue.html.php" );
			break;
		case 'callback':
			$('.myagent-wrapper').css("background-color","orange");//orange
			$( "#full-display-section" ).load( "html/inner.html.php", function() {
			$( "#upper-content-section" ).load( "html/phone.html.php", function() {
			$( "#lower-content-section" ).load( "html/history.html.php", function() {
			$( ".history-body").css("height","160px");
			// load successful
			});
			});
			})

			break;
		case 'break':
			$('.myagent-wrapper').css("background-color","#456a97"); //blue
			$( "#full-display-section" ).load( "html/address.html.php" );
			break;
		case 'wrap':
			$('.myagent-wrapper').css("background-color","#ff7332"); //

			$( "#full-display-section" ).load( "html/inner.html.php", function() {
			$( "#upper-content-section" ).load( "html/conversation.html.php", function() {
			$( "#lower-content-section" ).load( "html/feedback.html.php", function() {
			$( ".feedback-body").css('max-height','150px', function() {
			//load successfull
			});
			});
			});
			})

			// $( "#full-display-section" ).load( "html/inner.html.php");
			// jQuery.ajaxSetup({ async: false }); //if order matters
			// $.get("html/conversation.html.php", '', function (data) { $("#upper-content-section").append(data); });
			// $.get("html/feedback.html.php", '', function (data) { $("#lower-content-section").append(data); });
			// jQuery.ajaxSetup({ async: true });  //if order matters
			// $( "#feedback-wrapper").css('height','160px');
			// $( ".feedback-body").css('overflowY' ,'auto');
			
			break;
		case 'offline':
			$('.myagent-wrapper').css("background-color","#ccc"); //grey
			$('#full-display-section').empty();
			if(typeof(agentTimer) != "undefined" && agentTimer !== null) 
			{
				clearInterval(agentTimer);
			}
			break;
	}
}(setStatus())


function setTimer(selector) 
{
	var elapsed_seconds = 0;
	var timer = setInterval(function() 
	{
		elapsed_seconds = elapsed_seconds + 1;
		$(selector).text(get_elapsed_time_string(elapsed_seconds));
	}, 1000);
	return timer;
}

 function get_elapsed_time_string(total_seconds) 
 {
	  function pretty_time_string(num) 
	  {
	    	return ( num < 10 ? "0" : "" ) + num;
	  }

	  var hours = Math.floor(total_seconds / 3600);
	  total_seconds = total_seconds % 3600;

	  var minutes = Math.floor(total_seconds / 60);
	  total_seconds = total_seconds % 60;

	  var seconds = Math.floor(total_seconds);

	  // Pad the minutes and seconds with leading zeros, if required
	/*   hours = pretty_time_string(hours); */
	/*   minutes = pretty_time_string(minutes); */
	  seconds = pretty_time_string(seconds);

	  // Compose the string for display
	  if (hours == 0) 
	  {
	  	var currentTimeString =  minutes + ":" + seconds;	  
	  }
	 else
	 {
	 	 minutes = pretty_time_string(minutes);
		 var currentTimeString = hours + ":" + minutes + ":" + seconds;
	 } 

	  return currentTimeString;
}

function blink(selector) 
{
	$(selector).fadeOut('slow', function()
	{
		$(this).fadeIn('slow')
	});
};


(function($) {
    $.fn.selected = function(fn) {
        return this.each(function() {
            var clicknum = 0;
            $(this).click(function() {
                clicknum++;
                if (clicknum == 2) {
                    clicknum = 0;
                    fn(this);
                }
            });
        });
    }
})(jQuery);


// function paintList(selectorUL) 
// {
// $("selectorUL > li").filter(":nth-child(odd)").css("background", "#f2f2f2").end().filter(":nth-child(even)").css("background", "#e0e0e0");	
// }



function queueCounter() 
{
	// Update the count of the calls in queue
	$("#queue-count").text($('#queue-list li').size());
	if ($('#queue-list li').size() >= 5) 
	{
		blink('#queue-count');
	};
	if ($('#queue-list li').size() >= 10) 
	{
		$('#queue-count').css({"background-color":"red", "color":"white"});
	}
	else 
	{
		$('#queue-count').css({"background-color":"white", "color":"black"});
	};

}





// Queue


 // Update the count of the calls in queue
$("#queue-count").text($('#queue-list li').not('#empty').size());

// close the myagent when power button is pressed
$("#power-off").click(function () 
{
$('.container').fadeOut('100', function()
	{
		//function here
	});

});


 // Trigger a response to what status the user selects
$('.myagent').selected(function() {
    setStatus();
});


$('.agent-leg').on({ 'click': function() 
{
	var src = ($(this).attr('src') === 'images/red-phone.png')
	? 'images/green-phone.png'
	: 'images/red-phone.png';
	$(this).attr('src', src);  
}
});











