
$("#activate").click(function () 
{
    $('.container').toggle("drop", { direction: "left" }, 500);
    $('.settings').hide();
});


 // DEMO ONLY .. triggers a new call into the queue
$("#call-in").click(function () 
{
	$("#queue-list li").remove('#empty');
	$("#queue-list").append('<li>iFocus Radio <span class = "timer">0:00</span></li>');
	queueTimers.push(setTimer($('#queue-list li:last').find('.timer')));
	// Update the count of the calls in queue
	queueCounter();
	paintList();
});



// DEMO ONLY .. removes the top call from the queue list
$("#call-out").click(function () 
{
	$('#queue-list li:first').fadeOut("slow").remove();
	clearInterval(queueTimers.shift());
	queueCounter();
	if ($('#queue-count').text() == '0') 
	{
		$("#queue-list").append('<li id="empty"><center>Queue is empty</center></span></li>');
	}
	paintList();
});
